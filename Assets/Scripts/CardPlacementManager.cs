using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPlacementManager : MonoBehaviour
{
    [SerializeField]
    private DeckManager deckManager;
    [SerializeField]
    private GameObject freeSpacePrefab;

    private Grid grid;

    private Vector2Int selectedFreeSpace;
    
    public GameManager gameManager;  // Referencia na GameManager



    // Nová dočasná karta
    private GameObject tempCard;

   void Start()
    {
        // Initialize the grid
        grid = new Grid(100, 100);  // Set grid dimensions as needed
       

        PlaceInitialCard();

        // Potiahnutie prvej karty po umiestnení počiatočnej karty
        gameManager.CurrentPlayer.DrawCard();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null && hit.collider.gameObject.CompareTag("FreeSpace"))
            {
                selectedFreeSpace = WorldToGrid(hit.collider.gameObject.transform.position);
                // Kontrola, či už nie je karta umiestnená
                if (tempCard == null)
                {
                    tempCard = gameManager.CurrentPlayer.GetCard(); // Získanie karty od hráča
                    tempCard.SetActive(true); // Zobrazenie karty
                    tempCard.transform.position = GridToWorld(selectedFreeSpace);
                }
                else
                {
                    // Ak je karta už umiestnená, presunie sa na novú pozíciu
                    tempCard.transform.position = GridToWorld(selectedFreeSpace);
                }
            }
        }
    }

    void PlaceInitialCard()
    {
        if (deckManager.CardsInDeck > 0)
        {
            GameObject card = deckManager.GetRandomCard();
            Vector2Int gridPosition = new Vector2Int(grid.OriginX, grid.OriginY);
            card.transform.position = GridToWorld(gridPosition);
            grid.OccupySpace(gridPosition.x, gridPosition.y);
            GenerateFreeSpaces(gridPosition);
        }
    }


    void GenerateFreeSpaces(Vector2Int gridPosition)
    {
        Vector2Int[] directions = new Vector2Int[] {
            new Vector2Int(1, 0),
            new Vector2Int(-1, 0),
            new Vector2Int(0, 1),
            new Vector2Int(0, -1)
        };

        foreach (Vector2Int direction in directions)
        {
            Vector2Int potentialPosition = gridPosition + direction;

            // Check if there is already a card or free space at this position
            if (CanPlaceFreeSpace(potentialPosition))
            {
                GameObject newFreeSpace = Instantiate(freeSpacePrefab, GridToWorld(potentialPosition), Quaternion.identity);
                newFreeSpace.name = "FreeSpace " + potentialPosition;
            }
        }
    }

   bool CanPlaceFreeSpace(Vector2Int gridPosition)
    {
        // Check if the position is within the grid boundaries
        if (gridPosition.x < 0 || gridPosition.y < 0 || gridPosition.x >= grid.Width || gridPosition.y >= grid.Height)
        {
            return false;
        }

        Vector2 worldPos = GridToWorld(gridPosition);

        // Check if there's already a Card or FreeSpace at this position
        Collider2D existingObject = Physics2D.OverlapCircle(worldPos, 0.1f); // Use appropriate radius
        if (existingObject != null && (existingObject.CompareTag("Card") || existingObject.CompareTag("FreeSpace")))
        {
            return false;
        }

        // The position is free
        return true;
    }

    public void RotateCard()
    {
        if (tempCard != null && tempCard == gameManager.CurrentPlayer.GetCard() && gameManager.CurrentPlayer.IsCurrentPlayer())
        {
            tempCard.transform.Rotate(0, 0, 90); // Rotácia o 90 stupňov. Upravte podľa potreby.
            
        }
    }

    public void AcceptPlacement()
    {
        if (tempCard != null && tempCard == gameManager.CurrentPlayer.GetCard() && gameManager.CurrentPlayer.IsCurrentPlayer())
        {
            // Zmeníme tag z karty na "Card", aby sa stala súčasťou hry
            tempCard.tag = "Card";
            // Okupácia miesta na mriežke
            grid.OccupySpace(selectedFreeSpace.x, selectedFreeSpace.y);
            // Odstránenie voľného miesta
            Destroy(GameObject.Find("FreeSpace " + selectedFreeSpace));
            // Generovanie nových voľných miest
            GenerateFreeSpaces(selectedFreeSpace);
            // Reset tempCard pre ďalšiu kartu
            tempCard = null;
            // Potiahnutie novej karty pre hráča
            gameManager.isTurnOver = true;  // Pridajte túto riadku
            // Call the method to clear the player's card image
            gameManager.CurrentPlayer.ClearCardImage();
            gameManager.NextPlayer();  // Prejdite na ďalšieho hráča po potiahnutí karty

            
            
        }
    }


    void PlaceCard(Vector2Int gridPosition, GameObject card)
    {
        if (card != null)
        {
            card.transform.position = GridToWorld(gridPosition);
            grid.OccupySpace(gridPosition.x, gridPosition.y);
            Destroy(GameObject.Find("FreeSpace " + gridPosition));
            GenerateFreeSpaces(gridPosition);
            // Znovu aktivujte kartu, aby bola viditeľná
            card.SetActive(true);
        }
    }

    Vector2Int WorldToGrid(Vector3 worldPosition)
    {
        return new Vector2Int(Mathf.FloorToInt(worldPosition.x) + grid.OriginX, Mathf.FloorToInt(worldPosition.y) + grid.OriginY);
    }

    Vector3 GridToWorld(Vector2Int gridPosition)
    {
        return new Vector3(gridPosition.x - grid.OriginX, gridPosition.y - grid.OriginY, 0);
    }


}
