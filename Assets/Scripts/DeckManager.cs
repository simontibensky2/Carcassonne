using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckManager : MonoBehaviour
{
    [SerializeField]
    private Transform cardParentTransform;
    // Prefaby kariet
    public List<GameObject> cardPrefabsType1;
    public List<GameObject> cardPrefabsType2;
    public GameObject cardPrefabType3;
    public List<GameObject> cardPrefabsType4;

    // Zoznam kariet v balíčku
    private List<GameObject> deck = new List<GameObject>();

    void Start()
    {
        // Pri vytváraní balíka sa namiesto vytvorenia a umiestnenia kariet na scéne
        // len vytvoria ich prefaby a pridajú do zoznamu 'deck'
        deck = CreateDeck();
    }


    public List<GameObject> CreateDeck()
    {
        List<GameObject> newDeck = new List<GameObject>();
        
        // Add Type 1 Cards - 4 prefabs, each repeated 3 times = 12 cards
        foreach (var prefab in cardPrefabsType1)
        {
            for (int i = 0; i < 3; i++)
            {
                newDeck.Add(prefab);
            }
        }
        
        // Add Type 2 Cards - 4 prefabs, each repeated 3 times = 12 cards
        foreach (var prefab in cardPrefabsType2)
        {
            for (int i = 0; i < 3; i++)
            {
                newDeck.Add(prefab);
            }
        }
        
        // Add Type 3 Cards - 1 prefab repeated 4 times = 4 cards
        for (int i = 0; i < 4; i++)
        {
            newDeck.Add(cardPrefabType3);
        }
        
        // Add Type 4 Cards - 6 prefabs, each repeated 2 times = 12 cards
        foreach (var prefab in cardPrefabsType4)
        {
            for (int i = 0; i < 2; i++)
            {
                newDeck.Add(prefab);
            }
        }

        // Shuffle the deck
        for (int i = 0; i < newDeck.Count; i++)
        {
            GameObject temp = newDeck[i];
            int randomIndex = Random.Range(i, newDeck.Count);
            newDeck[i] = newDeck[randomIndex];
            newDeck[randomIndex] = temp;
        }

        return newDeck;
    }

    public GameObject GetRandomCard()
    {
        if (deck.Count > 0)
        {
            int index = Random.Range(0, deck.Count);
            GameObject cardToReturn = deck[index];
            deck.RemoveAt(index);

            // Instantiate the card at the parent transform
            return Instantiate(cardToReturn, cardParentTransform);
        }

        return null;
    }

    // Property for getting the number of cards left in the deck
    public int CardsInDeck
    {
        get { return deck.Count; }
    }
}
