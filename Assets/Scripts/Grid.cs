using UnityEngine;

public class Grid
{
    public int Width { get; private set; }
    public int Height { get; private set; }
    public int OriginX { get; private set; }
    public int OriginY { get; private set; }

    private bool[,] gridArray;

    public Grid(int width, int height)
    {
        Width = width;
        Height = height;
        OriginX = width / 2;  // Set the x origin to the center of the width
        OriginY = height / 2;  // Set the y origin to the center of the height
        gridArray = new bool[width, height];
    }

    public void OccupySpace(int x, int y)
    {
        x -= OriginX;
        y -= OriginY;

        if (x >= 0 && y >= 0 && x < Width && y < Height)
        {
            gridArray[x, y] = true;
        }
    }


    public void FreeSpace(int x, int y)
    {
        int gridX = x + OriginX;
        int gridY = y + OriginY;
        if (gridX >= 0 && gridY >= 0 && gridX < Width && gridY < Height)
        {
            gridArray[gridX, gridY] = false;
        }
    }

    public bool IsSpaceFree(int x, int y)
    {
        int gridX = x + OriginX;
        int gridY = y + OriginY;
        if (gridX >= 0 && gridY >= 0 && gridX < Width && gridY < Height)
        {
            return !gridArray[gridX, gridY];
        }
        return false;
    }
}

