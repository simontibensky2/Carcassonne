using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PathColor
{
    None,
    Red,
    Green,
    Blue,
    Yellow
}

public class CardPath : MonoBehaviour
{
    [System.Serializable]
    public class Edge
    {
        public bool isEndPoint; // Is this edge an endpoint (i.e., a "dead end")?
        public PathColor color; // The color of the path at this edge

        public Edge(bool isEndPoint, PathColor color)
        {
            this.isEndPoint = isEndPoint;
            this.color = color;
        }
    }

    public Edge topEdge;
    public Edge bottomEdge;
    public Edge leftEdge;
    public Edge rightEdge;

    void Start()
    {
        // Initialize the edges
        topEdge = new Edge(false, PathColor.None);
        bottomEdge = new Edge(false, PathColor.None);
        leftEdge = new Edge(false, PathColor.None);
        rightEdge = new Edge(false, PathColor.None);
    }

    public void Rotate(int angle)
    {
        int rotationSteps = angle / 90;

        for (int i = 0; i < rotationSteps; i++)
        {
            Edge oldTop = topEdge;
            topEdge = leftEdge;
            leftEdge = bottomEdge;
            bottomEdge = rightEdge;
            rightEdge = oldTop;
        }
    }

    public List<CardPath> connectedCards = new List<CardPath>(); // list of adjacent cards
    
    public void AddConnectedCard(CardPath card)
    {
        if (!connectedCards.Contains(card))
        {
            connectedCards.Add(card);
        }
    }

    public bool CheckClosedPath()
    {
        // This is a very basic check for a closed path. You might need a more complex check depending on your game rules.
        // This only checks if this card is connected to itself, which would mean a closed path.
        return connectedCards.Contains(this);
    }

    public Dictionary<PathColor, int> CalculatePoints()
    {
        Dictionary<PathColor, int> points = new Dictionary<PathColor, int>() 
        {
            { PathColor.Red, 0 },
            { PathColor.Green, 0 },
            { PathColor.Blue, 0 },
            { PathColor.Yellow, 0 }
        };
        
        if (CheckClosedPath())
        {
            foreach (CardPath card in connectedCards)
            {
                if (card.topEdge.color != PathColor.None) points[card.topEdge.color]++;
                if (card.bottomEdge.color != PathColor.None) points[card.bottomEdge.color]++;
                if (card.leftEdge.color != PathColor.None) points[card.leftEdge.color]++;
                if (card.rightEdge.color != PathColor.None) points[card.rightEdge.color]++;
            }
        }

        return points;
    }

}
