using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField]
    private Color playerColor;

    [SerializeField]
    private DeckManager deckManager;

    private Image cardImage;
    private List<GameObject> hand = new List<GameObject>();
    public GameManager gameManager;  // Referencia na GameManager
    
    [SerializeField]
    private Button rotateButton; // reference to the rotate button

    [SerializeField]
    private Button acceptButton; // reference to the accept button
    

    void Start()
    {
        GameObject cardObject = transform.Find("Card").gameObject;
        if (cardObject != null)
        {
            cardImage = cardObject.GetComponent<Image>();
        }
        else
        {
            Debug.LogError("No child GameObject named 'Card' found on " + gameObject.name);
        }
        
        DeactivateButtons();

    }
    
    public void ActivateButtons()
    {
        rotateButton.interactable = true;
        acceptButton.interactable = true;
    }

    public void DeactivateButtons()
    {
        rotateButton.interactable = false;
        acceptButton.interactable = false;
    }
    
    public bool IsCurrentPlayer()
    {
        return this == gameManager.CurrentPlayer;
    }

    // Vráti aktuálnu kartu hráča
    public GameObject GetCard()
    {
        if (hand.Count > 0)
        {
            return hand[hand.Count - 1];
        }
        else
        {
            Debug.LogError("No card in hand.");
            return null;
        }
    }


   public GameObject PlayCard()
    {
        if (hand.Count > 0)
        {
            GameObject card = hand[0];
            hand.RemoveAt(0);
            return card;
        }
        else
        {
            Debug.LogError("No cards in hand.");
            return null;
        }
    }

    public void ClearCardImage()
        {
            if (cardImage != null)
            {
                cardImage.sprite = null;  // Or set it to an empty card sprite
            }
        }
    public void DrawCard()
    {
        if(gameManager.isTurnOver && deckManager.CardsInDeck > 0)
        {
            GameObject card = deckManager.GetRandomCard();

            // Assign the name of the instantiated card (prefab name + "(Clone)") to the drawn card
            card.name = card.name.Replace("(Clone)", "").Trim();

            //Debug.Log("Player drew card: " + card.name);
            hand.Add(card);
            card.SetActive(false);

            Sprite cardSprite = Resources.Load<Sprite>(card.name);
            if (cardSprite)
            {
                cardImage.sprite = cardSprite; // set the sprite of the UI Image
                //Debug.Log("Card image set to: " + cardSprite.name);
            }
            else
            {
                Debug.LogError("No sprite with the name " + card.name + " found in Resources folders.");
            }
        }
        else
        {
            Debug.Log("No cards left in deck.");
        }
    }

}