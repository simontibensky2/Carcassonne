using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private List<Player> players = new List<Player>();  // Zoznam všetkých hráčov v hre
    private int currentPlayerIndex = 0;

    public Player CurrentPlayer { get => players[currentPlayerIndex]; }
    public bool isTurnOver = true;  // Pridajte túto riadku

    void Start()
    {
        // Ak existuje nejaký neaktívny hráč, odstráni ho zo zoznamu
        players.RemoveAll(player => player.gameObject.activeSelf == false);

        // Kontrola, či existujú nejakí hráči
        if (players.Count == 0)
        {
            Debug.LogError("No active players found.");
            return;
        }

        // Začíname s prvým hráčom
        CurrentPlayer.DrawCard();
        CurrentPlayer.ActivateButtons(); // Add this line
        Debug.Log("Player " + currentPlayerIndex);
    }

    public void NextPlayer()
    {
        if(isTurnOver)
        {
            // deactivate buttons for the current player
            CurrentPlayer.DeactivateButtons();

            currentPlayerIndex = (currentPlayerIndex + 1) % players.Count;  // Índex nasledujúceho hráča
            Debug.Log("Player " + currentPlayerIndex);
            // activate buttons for the new current player
            CurrentPlayer.ActivateButtons();

            CurrentPlayer.DrawCard();  // Nasledujúci hráč si ťahá kartu
            isTurnOver = false;
        }
    }
}