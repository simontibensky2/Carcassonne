using UnityEngine;

public class Card : MonoBehaviour
{
    public string color1; // Add a public variable to store the first color
    public string color2; // Add a public variable to store the second color
    public int type; // Add a public variable to store the card type

    // Method to set the type of card (as you previously had)
    public void SetType(int cardType)
    {
        type = cardType;
    }

    // Method to get the card type
    public int GetCardType()
    {
        return type;
    }

    // Method to set the colors of the card
    public void SetColors(string colorOne, string colorTwo)
    {
        color1 = colorOne;
        color2 = colorTwo;
    }

    // Method to get the first color of the card
    public string GetColor1()
    {
        return color1;
    }

    // Method to get the second color of the card
    public string GetColor2()
    {
        return color2;
    }

}
